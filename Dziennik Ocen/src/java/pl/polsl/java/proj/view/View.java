/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.proj.view;

import java.io.PrintWriter;

/**
 * Class is responsible for creating HTML site.
 *
 * @author Grzegorz Nowak
 * @version 1.5
 */
public class View {

    /**
     * PrintWriter class object.
     */
    private PrintWriter output;

    /**
     * Constructor.
     */
    public View(PrintWriter _output) {
        this.output = _output;
    }

    /**
     * Method starts HTML site.
     */
    public void startHTMLSite() {
        output.println("<html>\n<body>\n");
    }

    /**
     * Method display text on html site.
     *
     * @param text to display
     */
    public void showTextOnHTMLSite(String text) {
        output.println("<h1>"+text+"</h1>");
    }
    
    public void showSQLonHTMLSite(String query){
        output.println("<font face=\"consolas\">"+query+"</font><p></p>");
    }
    
    /**
     * Method ends HTML site.
     */
    public void endHTMLSite() {
        output.println("</body>\n</html>");
    }
}