/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.proj.login;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.polsl.java.proj.view.View;

/**
 * This class is a Servlet and responsible for user registration, overrides
 * method GET.
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class Registration extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method, checks correctness password and
     * save to file with logins and passwords.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {

        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String password2 = req.getParameter("password2");

        View view = new View(resp.getWriter());

        if (login.length() != 0 && password.length() != 0 && password2.length() != 0) {

            if (password.equals(password2)) {
                File file = new File("logins.txt");
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileWriter fileWriter = new FileWriter("logins.txt", true);
                BufferedWriter out = new BufferedWriter(fileWriter);
                out.write("\r\n" + login + " " + password);
                out.close();
                view.startHTMLSite();
                view.showTextOnHTMLSite("Account has been created!");
                view.showTextOnHTMLSite("<form> <input type=\"button\" value=\"Go back!\" style=\"height:25px; width:100px\" onclick=\"history.back()\"> </input> </form>");
                view.endHTMLSite();

            } else {
                view.startHTMLSite();
                view.showTextOnHTMLSite("Passwords are different!");
                view.endHTMLSite();
            }

        } else {
            view.startHTMLSite();
            view.showTextOnHTMLSite("You should give three parameters!");
            view.endHTMLSite();
        }

    }
}
