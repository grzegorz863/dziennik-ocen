/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.proj.login;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pl.polsl.java.proj.model.Controller;
import pl.polsl.java.proj.model.CreateTables;
import pl.polsl.java.proj.view.View;

/**
 * This class is a Servlet and responsible for log in, overrides method POST.
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class Login extends HttpServlet {

    /**
     * Collection of logins and passwords.
     */
    private Hashtable<String, String> log;

    /**
     * Constructor initiating Login class object.
     */
    public Login() {

    }

    /**
     * Handles the HTTP <code>POST</code> method, validate login and password
     * and create session object.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException {

        
        Connection connection = null;
        resp.setContentType("text/html; charset=ISO-8859-2");
        PrintWriter out = resp.getWriter();
        View view = new View(out);
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            connection = DriverManager.getConnection("jdbc:derby://localhost:1527/RatingLogDB", "lab", "lab");
            new CreateTables().createTable(connection);
            
            this.log = new ReadFromFile().readLogPass(req, resp);
            String login = req.getParameter("login");
            String password = req.getParameter("password");

            if (login.length() != 0 && password.length() != 0) {

                if (log.get(login) == null || !log.get(login).equals(password)) {
                    view.startHTMLSite();
                    view.showTextOnHTMLSite("You entered wrong password or you do not have account in our site");
                    view.endHTMLSite();
                    return;
                } else {
                    view.startHTMLSite();
                    view.showTextOnHTMLSite("Hello" + login);
                    view.endHTMLSite();
                    out.println();

                    HttpSession session = req.getSession(true);
                    Controller controller = new Controller(connection);
                    session.setAttribute("controller", controller);
                    req.getRequestDispatcher("/WEB-INF/menu.jsp").forward(req, resp);
                }

            } else {
                view.startHTMLSite();
                view.showTextOnHTMLSite("You should give two parameters!");
                view.endHTMLSite();
            }

        } catch (FileNotFoundException error) {
            view.startHTMLSite();
            view.showTextOnHTMLSite("Server error!");
            view.endHTMLSite();
        }catch(SQLException error){
            System.err.println("SQL exception: " + error.getMessage());
        }catch (ClassNotFoundException error) {
            System.err.println("ClassNotFound exception: " + error.getMessage());
        }
    }
}
