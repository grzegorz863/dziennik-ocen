/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.proj.login;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Scanner;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is responsible for read from file logins and passwords.
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class ReadFromFile {

    /**
     * Method read logins ans passwords from file and save it to Hashtable.
     *
     * @param req servlet request
     * @param response servlet response
     * @return Hashtable with logins (keys) and passwords.
     * @throws java.io.FileNotFoundException if file does not exist.
     * @throws IOException if an I/O error occurs.
     */
    public Hashtable<String, String> readLogPass(HttpServletRequest req, HttpServletResponse response) throws FileNotFoundException, IOException {

        Hashtable<String, String> log = new Hashtable<>();
        String fileName = "logins.txt";
        File file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
        }
        Scanner reading = new Scanner(file);
        try {

            String line = reading.nextLine();
            while (line != null) {
                String[] loginAndPassword = line.split(" ");
                if (loginAndPassword.length == 2) {
                    log.put(loginAndPassword[0], loginAndPassword[1]);
                }
                line = reading.nextLine();

            }
        } catch (NoSuchElementException e) {
            reading.close();
        }
        return log;
    }
}
