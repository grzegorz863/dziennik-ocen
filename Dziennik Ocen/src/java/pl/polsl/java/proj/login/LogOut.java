/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.proj.login;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.polsl.java.proj.model.Controller;
import pl.polsl.java.proj.view.View;

/**
 * This class is a Servlet and responsible for log out, overrides method GET.
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class LogOut extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method, when user click "LogOut"
     * button.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        
        Controller controller = (Controller) req.getSession().getAttribute("controller");
        //String fileName = "dataBase.txt";
        //controller.saveToFile(fileName);
        String fileName = "QueryHistory.txt";
        controller.saveSQLqueryToFile(fileName);
        controller.closeDBconection();
        req.getSession().invalidate();
        
        View view = new View(resp.getWriter());
        view.startHTMLSite();
        view.showTextOnHTMLSite("<meta http-equiv=\"refresh\" content=\"0; url=http://localhost:8080/DziennikOcen\" />");
        view.endHTMLSite();
    }
}
