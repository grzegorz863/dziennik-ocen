/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.proj.servlets;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.polsl.java.proj.model.Controller;
import pl.polsl.java.proj.view.View;

/**
 * This class is responsible for reading and displaying history of SQL query.
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class SQLhistory extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method, displays history of SQL query.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        View view = new View(resp.getWriter());
        try {
            Controller controller = (Controller) req.getSession().getAttribute("controller");
            int i = 0;
            view.startHTMLSite();

            try {
                //try{
                File file = new File("QueryHistory.txt");
                if (!file.exists()) {
                    file.createNewFile();
                }
                file = new File("QueryHistory.txt");
                Scanner input = new Scanner(file);
                //}catch(FileNotFoundException error){

                //}
                while (true) {
                    view.showSQLonHTMLSite(input.nextLine());
                }
            } catch (NoSuchElementException error) {
            }

            while (true) {
                view.showSQLonHTMLSite(controller.getQueryHistory(i));
                i++;
            }

        } catch (IndexOutOfBoundsException error) {
            view.endHTMLSite();
        }
    }

}
