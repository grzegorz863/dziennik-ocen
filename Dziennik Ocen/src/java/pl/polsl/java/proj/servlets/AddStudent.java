/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.proj.servlets;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.polsl.java.proj.model.Controller;
import pl.polsl.java.proj.view.View;

/**
 * This class is a Servlet and responsible for add students, overrides method
 * GET.
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class AddStudent extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method, add students to rating log.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {

        View view = new View(resp.getWriter());
        try {
            Controller controller = (Controller) req.getSession().getAttribute("controller");

            String name = req.getParameter("name");
            String surname = req.getParameter("surname");

            if (name.length() != 0 && surname.length() != 0) {
                view.startHTMLSite();
                view.showTextOnHTMLSite(controller.addStudent(name, surname));
                view.endHTMLSite();

            } else {
                view.startHTMLSite();
                view.showTextOnHTMLSite("You should give two parameters!");
                view.endHTMLSite();
            }
        } catch (NullPointerException error) {
            view.startHTMLSite();
            view.showTextOnHTMLSite("<meta http-equiv=\"refresh\" content=\"0; url=http://localhost:8080/DziennikOcen\" />");
            view.endHTMLSite();
        } catch (SQLException error) {
            view.startHTMLSite();
            view.showTextOnHTMLSite("Database error occurred!");
            view.endHTMLSite();
        }
    }
}
