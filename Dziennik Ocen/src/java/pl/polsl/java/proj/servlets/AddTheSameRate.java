/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.proj.servlets;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.polsl.java.proj.model.Controller;
import pl.polsl.java.proj.view.View;

/**
 * This class is a Servlet and responsible for add the same rate to all users,
 * overrides method GET.
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class AddTheSameRate extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method, add the same rate to all users
     * in rating log.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {

        View view = new View(resp.getWriter());
        try {
            Controller controller = (Controller) req.getSession().getAttribute("controller");

            String rate = req.getParameter("rate");

            try {
                if (rate.length() != 0) {
                    view.startHTMLSite();
                    view.showTextOnHTMLSite(controller.addRateToAllStudents(rate));
                    view.endHTMLSite();

                } else {
                    view.startHTMLSite();
                    view.showTextOnHTMLSite("You should give one parameters!");
                    view.endHTMLSite();
                }
            } catch (NumberFormatException error) {
                view.startHTMLSite();
                view.showTextOnHTMLSite("Value in Rate field is NaN");
                view.endHTMLSite();
            }
        } catch (NullPointerException error) {
            view.startHTMLSite();
            view.showTextOnHTMLSite("<meta http-equiv=\"refresh\" content=\"0; url=http://localhost:8080/DziennikOcen\" />");
            view.endHTMLSite();
        } catch (SQLException error) {
            view.startHTMLSite();
            view.showTextOnHTMLSite("Database error occurred!");
            view.endHTMLSite();
        }
    }
}
