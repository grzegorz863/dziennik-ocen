/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.proj.model;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This class is responsible for createing tables in data base. 
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class CreateTables {
    
    /**
     * Creates table in data base
     * @param con connection
     */
    public void createTable(Connection con){
        
        try {
            Statement statement = con.createStatement();

            statement.executeUpdate("CREATE TABLE Rate " +
                    "(IDRate INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY, "
                    + "VALUEOFRATE DECIMAL(2,1), IDStudent INTEGER)");
            statement.executeUpdate("CREATE TABLE Student " +
                    "(IDStudent INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY, "
                    + "NAMEOFSTUDENT VARCHAR(50), surname VARCHAR(50))");
            System.out.println("Table created");
            
            statement.executeUpdate("ALTER TABLE Rate add foreign key (IDStudent) references Student(IDStudent) ON DELETE CASCADE");
                       
            
        } catch (SQLException sqle) {
            System.err.println("SQL exception: " + sqle.getMessage());
        }  
    }
}
