/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.proj.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pl.polsl.java.proj.exceptions.StudentNotFoundException;

/**
 * This class is responsible for redading and save files, controll input data
 * and controll server operating.
 *
 * @author Grzegorz Nowak
 * @version 1.3
 */
public class Controller {

    /**
     * Conection object declaration.
     */
    private Connection connection;

    /**
     * ArrayList with SQL query history.
     */
    private List<String> queryHistory = new ArrayList<>();

    /**
     * Constructor.
     */
    public Controller(Connection con) {
        this.connection = con;
    }

    /**
     * Method executes SQL query and returns ratings of selected student.
     *
     * @param name student's name.
     * @param surname student's surname.
     * @return All student ratings
     * @throws pl.polsl.java.proj.exceptions.StudentNotFoundException Student
     * not found exception
     * @throws java.sql.SQLException error in SQL
     */
    public String showAllStudentRatings(String name, String surname) throws StudentNotFoundException, SQLException {
        String ratings = "";
        Statement statement = connection.createStatement();
        String query = "select s.IDSTUDENT from STUDENT s where s.NAMEOFSTUDENT = '" + name + "' and s.SURNAME = '" + surname + "'";
        ResultSet rs = statement.executeQuery(query);
        queryHistory.add(query);
        if (!rs.next()) {
            throw new StudentNotFoundException("Student not found");
        }
        query = "select r.VALUEOFRATE from Student s join Rate r on s.IDSTUDENT = r.IDSTUDENT where s.NAMEOFSTUDENT = '" + name + "' and s.SURNAME = '" + surname + "'";
        rs = statement.executeQuery(query);
        queryHistory.add(query);
        while (rs.next()) {
            ratings = ratings + rs.getString("VALUEOFRATE") + ' ';
        }
        ratings = name + ' ' + surname + ' ' + ratings;
        return ratings;
    }

    /**
     * Method executes SQL query and adds studets to rating log data base.
     *
     * @param name student's name.
     * @param surname student's surname.
     * @return final comunicate about end's of this method.
     * @throws java.sql.SQLException error in SQL
     */
    public String addStudent(String name, String surname) throws SQLException {
        Statement statement = connection.createStatement();
        String query = "select s.IDSTUDENT from STUDENT s where s.NAMEOFSTUDENT = '" + name + "' and s.SURNAME = '" + surname + "'";
        ResultSet rs = statement.executeQuery(query);
        queryHistory.add(query);
        if (rs.next()) {
            return "A student with the given name and surname already exists!";
        }
        query = "INSERT INTO Student VALUES (default, '" + name + "', '" + surname + "')";
        statement.executeUpdate(query);
        queryHistory.add(query);
        return "Student has been added!";
    }

    /**
     * Method executes SQL query and adds rate to selected student in data base.
     *
     * @param name student's name.
     * @param surname student's surname.
     * @param s_rate student's rate.
     * @return final comunicate about end's of this method
     * @throws pl.polsl.java.proj.exceptions.StudentNotFoundException Student
     * not found exception.
     * @throws NumberFormatException rate is not a number.
     * @throws java.sql.SQLException error in SQL
     */
    public String addRateToSelectedStudent(String name, String surname, String s_rate) throws StudentNotFoundException, NumberFormatException, SQLException {
        Double d_rate = Double.parseDouble(s_rate);
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM Student s WHERE s.nameOfStudent='" + name + "' and s.surname='" + surname + "'";
        ResultSet rs = statement.executeQuery(query);
        queryHistory.add(query);
        if (!rs.next()) {
            throw new StudentNotFoundException("Student not found");
        }
        if ((d_rate >= 2) && (d_rate <= 5)) {
            query = "INSERT INTO Rate VALUES (default, " + d_rate + ", " + rs.getInt("IDStudent") + ")";
            statement.executeUpdate(query);
            queryHistory.add(query);
            return "Rate has been added!";
        } else {
            return "Wrong rate!";
        }
    }

    /**
     * Method executes SQL query and adds the same rate to all students in data
     * base.
     *
     * @param s_rate student's rate.
     * @return final comunicate about end's of this method.
     * @throws NumberFormatException rate is not a number.
     * @throws java.sql.SQLException
     */
    public String addRateToAllStudents(String s_rate) throws NumberFormatException, SQLException {
        Double d_rate = Double.parseDouble(s_rate);
        Statement statement = connection.createStatement();
        Statement statement2 = connection.createStatement();
        String query = "select s.IDSTUDENT from Student s";
        ResultSet rs = statement.executeQuery(query);
        queryHistory.add(query);
        if ((d_rate >= 2) && (d_rate <= 5)) {
            while (rs.next()) {
                query = "INSERT INTO Rate VALUES (default, " + d_rate + " , " + rs.getInt("IDStudent") + ")";
                statement2.executeUpdate(query);
                queryHistory.add(query);
            }
            return "Rates have been added!";
        } else {
            return "Wrong rate!";
        }

    }

    /**
     * Close connection with DB.
     */
    public void closeDBconection() {
        try {
            this.connection.close();
        } catch (SQLException ex) {
            System.err.println("Problem with closing the connection!");
        }
    }

    /**
     * Close connection with DB.
     *
     * @param index index of query.
     * @return this query.
     */
    public String getQueryHistory(int index) {
        return this.queryHistory.get(index) + "\n\r";
    }

    /**
     * Close connection with DB.
     * @param fileName file name.
     * @throws java.io.IOException file error.
     */
    public void saveSQLqueryToFile(String fileName) throws IOException {
        File file = new File(fileName);
        BufferedWriter out = null;
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fileWriter = new FileWriter("QueryHistory.txt", true);
            out = new BufferedWriter(fileWriter);
            int i = 0;
            while (true) {
                out.write(this.getQueryHistory(i) + "\r\n");
                i++;
            }
        } catch (IndexOutOfBoundsException error) {

            out.close();
        } catch (IOException error) {

        }
    }
}
